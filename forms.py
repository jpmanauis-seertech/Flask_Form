from flask import Flask, g, flash
from flask import request, render_template
from pymongo import MongoClient
from flask import redirect
#from wtforms import Form, validators, SelectField, ValidationError
from inventory import InventoryForm
from config import MyDatabase
from cart import CartDetails

DbClass = MyDatabase()
myDb = DbClass.createConn()

app = Flask(__name__)
app.secret_key = "training"

@app.route("/", methods = ['POST','GET'])
def miniApp():

	if "quantity" in request.form:
		qty = request.form["quantity"]
		item = request.form["item"]
		errMess = DbClass.updateInventory(myDb,qty,item)

	item_inv = DbClass.getItemList(myDb)

	return render_template('inventory.html', itemList=item_inv)

@app.route("/viewcart", methods = ['POST','GET'])
def cartView():

	cart = CartDetails()

	if request.method == 'POST':
		item = request.form["item"]
		DbClass.removeFromCart(myDb,item)

	cart_data,total_price = cart.getCartInformation(myDb)

	return render_template('cart.html',cartList = cart_data, total = total_price)

@app.route("/additem", methods = ['POST','GET'])
def addItem():


	if request.method == 'POST':
		inv = InventoryForm(request.form)
		for info in inv:
			print info
		if inv.validate() == False:
			return render_template("additem.html", forms = inv)
		else:
			##INSERT NEW ITEMS:
			errMess = DbClass.insertDataForm(myDb,inv)
			inv = InventoryForm()
			return render_template("additem.html", forms = inv, messages = errMess)
			
	elif request.method == 'GET':
		inv = InventoryForm()
		return render_template("additem.html", forms = inv)

if __name__ == '__main__':
	app.run(debug = True)
