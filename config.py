from flask import Flask,g,flash
from pymongo import MongoClient

class MyDatabase():
	def createConn(self):
		client = MongoClient()
		myDb = client.my_miniapp

		return myDb

	def insertDataForm(self,dbCon,form):

		##CHECK IF ITEM ALREADY EXISTS
		checker = dbCon.myinventory.find_one({'item': str(form.item.data)})
		
		if checker:
			errMess = "item already exist"
			return errMess

		myHash = {}
		myHash['item'] = str(form.item.data)
		myHash['stock'] = int(form.stock.data)
		myHash['Price'] = int(form.Price.data)

		errMess = "Successfully added a new item."

		dbCon.myinventory.insert(myHash)

		return errMess

	def getItemList(self,dbCon):
		row_list = dbCon.myinventory.find()
		return dbCon.myinventory.find()

		#postlist = dbCon.myinventory.find()
		#postDict = {}
		#for post in postlist:
		#	postDict['post']
		
	def removeFromCart(self,dbCon,item):

		##GET INFO OF ITEM TO REMOVE
		item_info = dbCon.my_cart.find_one({"item":item})
		item_info2 = dbCon.myinventory.find_one({"item":item})

		new_stock = int(item_info["Qty"]) + int(item_info2["stock"])

		##DELETE CART ENTRY
		dbCon.my_cart.remove({"item":item}) 

		##UPDATE INVENTORY
		dbCon.myinventory.update_one(
			{
			"item":item
			},{
			"$set":{
				"stock":int(new_stock)
				}
			}
		)

	def updateInventory(self,dbCon,qty,item):

		##GET REMAINING STOCK OF ITEM
		data = dbCon.myinventory.find_one({"item" : item})

		##CHECK IF STOCK IS LESS THAN QUANTITY
		if int(data["stock"]) < int(qty):
			errMess = "Insufficient stock for order quantity"
			return errMess

		remaining_stock = int(data["stock"]) - int(qty)

		dbCon.myinventory.update_one({"item":item},{"$set":{"stock":int(remaining_stock)}})

		##CHECK IF CART NEEDS TO INSERT OR UPDATE
		cart_info = dbCon.my_cart.find_one({"item":item})
		if not cart_info:
			##INSERT INTO CART TABLE
			dbCon.my_cart.insert(
				{
				"item" : item,
				"Qty" : int(qty),
				"Price" : data["Price"]
				}
			)
		else:
			##UPDATE CART TABLE
			newQty = int(cart_info["Qty"]) + int(qty)
			dbCon.my_cart.update_one(
				{"item":item},
				{"$set": {
					"Qty": newQty
					}
				}
			)

