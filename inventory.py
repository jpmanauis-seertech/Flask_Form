from flask_wtf import Form
from wtforms import Form, validators, SelectField, ValidationError, TextField, TextAreaField, IntegerField, SubmitField
from pymongo import MongoClient
from config import MyDatabase

DbClass = MyDatabase()
myDb = DbClass.createConn()

class InventoryForm(Form):

	item = TextAreaField("Item Name",[validators.Required("Please input item.")])

	stock = IntegerField("Stock",[validators.Required("Please input item stock.")])

	Price = IntegerField("Item Price",[validators.Required("Please input price of item.")])

	submit = SubmitField("Add Item")


